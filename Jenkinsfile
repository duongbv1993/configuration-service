pipeline {
    agent any
    stages {
        stage('Checkout code') {
            steps {
                checkout scm
            }
        }
        stage('Build') {
            steps {
                sh 'mvn clean install'
            }
        }
        stage('Build Image') {
            when {
                branch 'master'
            }
            steps {
                sh 'docker build -t configuration-service:latest .'
            }
        }
        stage('Push Image') {
            when {
                branch 'master'
            }
            steps {
                sh 'docker tag configuration-service:latest 820712154160.dkr.ecr.ap-southeast-1.amazonaws.com/configuration-service:latest'
                sh 'aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin 820712154160.dkr.ecr.ap-southeast-1.amazonaws.com'
                sh 'docker push 820712154160.dkr.ecr.ap-southeast-1.amazonaws.com/configuration-service:latest'
            }
        }
        stage('Deploy') {
            when {
                branch 'master'
            }
            steps {
                script {
                    def clusterName = 'bsc-cluster-1' //jenkins variable later
                    def taskFamily  = 'configuration-service' //jenkins variable later
                    def serviceName = 'configuration-service' //jenkins variable later
                    // Get current [TaskDefinition#revision-number]
                    def currTaskDef = sh (
                        returnStdout: true,
                        script:  "aws ecs describe-task-definition  --task-definition ${taskFamily}     \
                                                                    | egrep 'revision'                  \
                                                                    | tr ',' ' '                        \
                                                                    | awk '{print \$2}'                 \
                      "
                    ).trim()

                    def currentTask = sh (
                        returnStdout: true,
                        script:  "aws ecs list-tasks  --cluster ${clusterName}                \
                                            --family ${taskFamily}                            \
                                            --output text                                     \
                                            | egrep 'TASKARNS'                                \
                                            | awk '{print \$2}'                               \
                      "
                    ).trim()

                    if(currTaskDef) {
                        sh  "aws ecs update-service  --cluster ${clusterName}             \
                                            --service ${serviceName}                      \
                                            --task-definition ${taskFamily}:${currTaskDef}\
                                            --desired-count 0                             \
                        "
                    }
                    if (currentTask) {
                        sh "aws ecs stop-task --cluster ${clusterName} --task ${currentTask}"
                    }

                    // Get the last registered [TaskDefinition#revision]
                    def taskRevision = sh (
                        returnStdout: true,
                        script:  "aws ecs describe-task-definition  --task-definition ${taskFamily}     \
                                                                    | egrep 'revision'                  \
                                                                    | tr ',' ' '                        \
                                                                    | awk '{print \$2}'                 \
                        "
                    ).trim()

                    // ECS update service to use the newly registered [TaskDefinition#revision]
                    //
                    sh  "aws ecs update-service  --cluster ${clusterName}                     \
                                              --service ${serviceName}                        \
                                              --task-definition ${taskFamily}:${taskRevision} \
                                              --desired-count 1                               \
                    "
                }
            }
        }
    }
}