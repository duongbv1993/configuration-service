FROM adoptopenjdk/openjdk11:alpine-jre
COPY target/bsc-centralize-configuration-1.0-SNAPSHOT.jar configuration-service.jar
ENTRYPOINT ["java","-jar","/configuration-service.jar"]